import { Module } from '@nestjs/common';
import { WorkoutDayService } from './workout-day.service';
import { WorkoutDayResolver } from './workout-day.resolver';
import { WorkoutDay, WorkoutDaySchema } from './entities/workout-day.entity';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: WorkoutDay.name, schema: WorkoutDaySchema },
    ])
  ],
  providers: [WorkoutDayResolver, WorkoutDayService]
})
export class WorkoutDayModule {}
