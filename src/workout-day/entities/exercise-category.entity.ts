import { ObjectType, Field } from '@nestjs/graphql';
import { Prop, Schema } from '@nestjs/mongoose';
import { Exercise } from './exercise.entity';

@Schema()
@ObjectType()
export class ExerciseCategory {
    @Field()
    @Prop({ required: true })
    name: String;

    @Field()
    @Prop({ required: true })
    order: number;

    @Field(type => [Exercise])
    @Prop({ required: true })
    exercises: Exercise[];
}
