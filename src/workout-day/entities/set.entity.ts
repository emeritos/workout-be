import { Field, Float, Int, ObjectType } from '@nestjs/graphql';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema()
@ObjectType()
export class Set {
    @Field()
    @Prop({ required: true })
    reps: number;

    @Field()
    @Prop({ required: true, type: Float })
    weight: number;

    @Field()
    @Prop({ required: true, type: Int })
    rest: number;

    @Field()
    @Prop({ required: true, type: Int })
    order: number;

    @Field()
    @Prop({ required: true })
    completed: boolean;
}
