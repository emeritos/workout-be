import { ObjectType, Field } from '@nestjs/graphql';
import { Prop, Schema } from '@nestjs/mongoose';
import { Set } from './set.entity';

@Schema()
@ObjectType()
export class Exercise {
    @Field()
    @Prop({ required: true })
    name: String;

    @Field(type => [Set])
    @Prop({ required: true })
    sets: Set[];
    
    @Field()
    @Prop({ required: true })
    order: number;
}



