import { ObjectType, Field, Int, ID } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Day } from 'src/enums/day.enum';
import { ExerciseCategory } from './exercise-category.entity';

@Schema()
@ObjectType()
export class WorkoutDay extends Document {

  @Field(type => ID)
  _id: String;

  @Field(type => Int)
  @Prop({ required: true })
  weekOfDay: Day;

  @Field({nullable: true})
  @Prop({ required: false })
  userId: String;

  @Field(type => [ExerciseCategory])
  @Prop()
  categories: ExerciseCategory[];
}

export const WorkoutDaySchema = SchemaFactory.createForClass(WorkoutDay);