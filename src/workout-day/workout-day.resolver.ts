import { Resolver, Query, Mutation, Args, Int, ID } from '@nestjs/graphql';
import { WorkoutDayService } from './workout-day.service';
import { WorkoutDay } from './entities/workout-day.entity';
import { CreateWorkoutDayInput } from './dto/create-workout-day.input';
import { UpdateWorkoutDayInput } from './dto/update-workout-day.input';

@Resolver(() => WorkoutDay)
export class WorkoutDayResolver {
  constructor(private readonly workoutDayService: WorkoutDayService) {}

  @Mutation(() => WorkoutDay)
  createWorkoutDay(@Args('createWorkoutDayInput') createWorkoutDayInput: CreateWorkoutDayInput) {
    return this.workoutDayService.create(createWorkoutDayInput);
  }

  @Query(() => [WorkoutDay], { name: 'workoutDays' })
  findAll() {
    return this.workoutDayService.findAll();
  }

  @Query(() => WorkoutDay, { name: 'workoutDay' })
  findOne(@Args('id', { type: () => ID }) id: string) {
    return this.workoutDayService.findOne(id);
  }

  @Mutation(() => WorkoutDay)
  updateWorkoutDay(@Args('updateWorkoutDayInput') updateWorkoutDayInput: UpdateWorkoutDayInput) {
    return this.workoutDayService.update(updateWorkoutDayInput._id, updateWorkoutDayInput);
  }

  @Mutation(() => WorkoutDay)
  removeWorkoutDay(@Args('id', { type: () => ID }) id: string) {
    return this.workoutDayService.remove(id);
  }
}
