import { InputType, Int, Field } from '@nestjs/graphql';
import { IsInt, Max, Min } from 'class-validator';
import { Day } from 'src/enums/day.enum';
import { CreateExerciseCategoryInput } from './create-exercise-category.input';

@InputType()
export class CreateWorkoutDayInput {

    @IsInt()
    @Min(Day.Monday)
    @Max(Day.Sunday)
    @Field(type => Int)
    weekOfDay: Day;

    @Field(type => [CreateExerciseCategoryInput])
    categories: CreateExerciseCategoryInput[];
}

