import { InputType, Field } from '@nestjs/graphql';
import { IsInt, IsString } from 'class-validator';
import { CreateSetInput } from './create-set.input';

@InputType()
export class CreateExerciseInput {

    @IsString()
    @Field()
    name: String;

    @Field(type => [CreateSetInput])
    sets: CreateSetInput[];

    @IsInt()
    @Field()
    order: number;
}

