import { InputType, Field } from '@nestjs/graphql';
import { IsBoolean, IsInt, IsNumber } from 'class-validator';

@InputType()
export class CreateSetInput {

    @IsInt()
    @Field()
    reps: number;

    @IsNumber()
    @Field()
    weight: number;

    @IsInt()
    @Field()
    rest: number;

    @IsInt()
    @Field()
    order: number;

    @IsBoolean()
    @Field()
    completed: boolean;
}

