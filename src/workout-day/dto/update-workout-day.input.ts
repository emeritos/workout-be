import { InputType, Field, PartialType, ID } from '@nestjs/graphql';
import { IsMongoId, IsString } from 'class-validator';
import { CreateWorkoutDayInput } from './create-workout-day.input';

@InputType()
export class UpdateWorkoutDayInput extends PartialType(CreateWorkoutDayInput) {

    @IsString()
    @IsMongoId()
    @Field(type => ID)
    _id: string;
}

