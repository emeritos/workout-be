import { InputType, Field } from '@nestjs/graphql';
import { IsInt, IsString } from 'class-validator';
import { CreateExerciseInput } from './create-exercise.input';

@InputType()
export class CreateExerciseCategoryInput {

    @Field()
    @IsString()
    name: String;

    @Field()
    @IsInt()
    order: number;

    @Field(type => [CreateExerciseInput])
    exercises: CreateExerciseInput[];
}

