import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateWorkoutDayInput } from './dto/create-workout-day.input';
import { UpdateWorkoutDayInput } from './dto/update-workout-day.input';
import { WorkoutDay } from './entities/workout-day.entity';

@Injectable()
export class WorkoutDayService {
  constructor(
    @InjectModel(WorkoutDay.name) private workoutDayModel: Model<WorkoutDay>
  ) { }

  async create(createWorkoutDayInput: CreateWorkoutDayInput): Promise<WorkoutDay> {
    const workoudDay = new this.workoutDayModel(createWorkoutDayInput);
    return await workoudDay.save();
  }

  async findAll(): Promise<WorkoutDay[]> {
    return await this.workoutDayModel.find();
  }

  async findOne(id: string): Promise<WorkoutDay> {
    return await this.workoutDayModel.findById(id);
  }

  async update(id: string, updateWorkoutDayInput: UpdateWorkoutDayInput): Promise<WorkoutDay> {
    return await this.workoutDayModel.findByIdAndUpdate(id, updateWorkoutDayInput);
  }

  async remove(id: string): Promise<WorkoutDay> {
    return await this.workoutDayModel.findByIdAndRemove(id);
  }
}
