import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { WorkoutDayModule } from './workout-day/workout-day.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      introspection: true,
      playground: true
    }),
    MongooseModule.forRoot(process.env.MONGODB_URL),
    WorkoutDayModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
